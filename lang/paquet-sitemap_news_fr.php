<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'sitemap_news_description' => 'Pour être indexé plus facilement dans Google News, il est préférable de fournir un fichier Sitemap dédié aux actualités, dont la documentation est disponible : https://developers.google.com/search/docs/crawling-indexing/sitemaps/news-sitemap?rd=2&hl=fr',
	'sitemap_news_nom' => 'Sitemap d’actualités',
	'sitemap_news_slogan' => 'Fournit un fichier Sitemap dédié aux actualités du site',
);
